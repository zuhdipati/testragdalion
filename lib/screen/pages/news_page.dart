import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:news_app/core/app_routes.dart';
import 'package:news_app/core/colors.dart';
import 'package:news_app/models/news_model.dart';
import 'package:news_app/services/api_service.dart';

class News extends StatelessWidget {
  const News({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final String judul = Get.arguments as String;

    var controller = Get.put(Service());

    controller.getData(judul);

    return Scaffold(
      appBar: AppBar(
          backgroundColor: ColorsApp.primaryColor,
          centerTitle: true,
          leading: InkWell(
              onTap: () {
                Get.back();
              },
              child: const Icon(Icons.arrow_back_ios_new)),
          title: Text("$judul News"),
          actions: [
            Container(
              margin: const EdgeInsets.only(right: 20),
              width: 30,
              height: 30,
              child: CircleAvatar(
                backgroundColor: Colors.white,
                child: ClipOval(child: Container()
                    //  Image.asset(
                    // 'assets/us_flag.png',
                    // fit: BoxFit.cover,
                    // ),
                    ),
              ),
            ),
          ]),
      body: Obx(
        () => controller.articles.isEmpty
            ? const Center(child: CircularProgressIndicator())
            : Column(
                children: [
                  const Padding(
                    padding: EdgeInsets.fromLTRB(20, 10, 20, 10),
                    child: TextField(
                      decoration: InputDecoration(
                        hintText: 'Search articles..',
                        suffixIcon: Icon(Icons.search),
                        border: OutlineInputBorder(),
                      ),
                    ),
                  ),
                  Expanded(
                    child: ListView.builder(
                      itemCount: controller.articles.length,
                      itemBuilder: (context, index) {
                        Articles article = controller.articles[index];
                        return InkWell(
                          onTap: () {
                            // String judul = category[index];
                            Get.toNamed(Routes.detail, arguments: article.url);
                          },
                          child: Container(
                            margin: const EdgeInsets.fromLTRB(20, 10, 20, 10),
                            width: MediaQuery.of(context).size.width,
                            height: 160,
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(10),
                                color: ColorsApp.secondaryColor),
                            child: Padding(
                              padding: const EdgeInsets.all(10.0),
                              child: Column(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text(
                                    article.title!,
                                    style: const TextStyle(
                                      fontWeight: FontWeight.bold,
                                      fontSize: 18,
                                    ),
                                  ),
                                  Column(
                                    children: [
                                      Column(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: [
                                          Text(
                                              article.author == null
                                                  ? "."
                                                  : article.author!,
                                              style: const TextStyle(
                                                  color: Colors.blue)),
                                          Text(article.publishedAt!)
                                        ],
                                      )
                                    ],
                                  )
                                ],
                              ),
                            ),
                          ),
                        );
                      },
                    ),
                  ),
                ],
              ),
      ),
    );
  }
}
