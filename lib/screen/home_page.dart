import 'package:flutter/material.dart';
import 'package:news_app/core/app_routes.dart';
import 'package:news_app/core/colors.dart';
import 'package:get/get.dart';

class HomePage extends StatelessWidget {
  const HomePage({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
          backgroundColor: ColorsApp.primaryColor,
          centerTitle: true,
          title: const Text("News Category"),
          actions: [
            Container(
              margin: const EdgeInsets.only(right: 20),
              width: 30,
              height: 30,
              child: CircleAvatar(
                backgroundColor: Colors.white,
                child: ClipOval(child: Container()
                    //  Image.asset(
                    // 'assets/us_flag.png',
                    // fit: BoxFit.cover,
                    // ),
                    ),
              ),
            ),
          ]),
      body: GridView.builder(
        shrinkWrap: true,
        itemCount: category.length,
        gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
          crossAxisCount: 3,
        ),
        itemBuilder: (context, index) {
          return InkWell(
            onTap: () {
              String judul = category[index];
              Get.toNamed(Routes.news, arguments: judul);
            },
            child: Padding(
              padding: const EdgeInsets.only(top: 20),
              child: Column(
                children: [
                  Container(
                    width: 60,
                    height: 60,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(10),
                      color: ColorsApp.primaryColor,
                    ),
                    child: const Center(
                      child: Icon(
                        Icons.newspaper,
                        color: Colors.white,
                        size: 25,
                      ),
                    ),
                  ),
                  const SizedBox(
                    height: 10,
                  ),
                  Text(
                    category[index],
                    style: const TextStyle(color: Colors.black),
                  )
                ],
              ),
            ),
          );
        },
      ),
    );
  }
}

List category = [
  "Business",
  "Entertainment",
  "General",
  "Health",
  "Science",
  "Sports",
  "Technology",
];
