import 'package:get/get.dart';
import 'package:http/http.dart' as http;
import 'dart:convert' as convert;

import 'package:news_app/models/news_model.dart';
import 'package:news_app/services/constants.dart' as constants;

class Service extends GetxController {
  var articles = [].obs;

  Future<void> getData(String category) async {
    String url;
    if (category == "Business") {
      url = constants.business;
    } else if (category == "Entertainment") {
      url = constants.entertainment;
    } else if (category == "General") {
      url = constants.general;
    } else if (category == "Health") {
      url = constants.health;
    } else if (category == "Science") {
      url = constants.science;
    } else if (category == "Sports") {
      url = constants.sports;
    } else if (category == "Technology") {
      url = constants.technology;
    } else {
      return;
    }

    final response = await http.get(Uri.parse(url));
    if (response.statusCode == 200) {
      final jsonData = convert.jsonDecode(response.body);
      final news = News.fromJson(jsonData);
      articles.value = news.articles!;
    } else {
      throw Exception('Failed to fetch news');
    }
  }

  @override
  void onInit() {
    String category = Get.arguments as String;
    getData(category);
    super.onInit();
  }
}
